/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, {useEffect, useState} from 'react';
import {
  ActivityIndicator,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
} from 'react-native';
import {Camera, useCameraDevices} from 'react-native-vision-camera';
import {BarcodeFormat, useScanBarcodes} from 'vision-camera-code-scanner';

import {Colors, Header} from 'react-native/Libraries/NewAppScreen';

const App = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  const [status, setStatus] = useState('');
  const devices = useCameraDevices();
  const device = devices.back;
  const [frameProcessor, barcodes] = useScanBarcodes([BarcodeFormat.QR_CODE], {
    checkInverted: true,
  });
  const [displayBarcode, setDisplayBarcode] = useState('');

  useEffect(() => {
    const init = async () => {
      try {
        const newCameraPermission = await Camera.requestCameraPermission();
        console.log('newCameraPermission', newCameraPermission);
        const cameraPermission = await Camera.getCameraPermissionStatus();
        setStatus(cameraPermission);
      } catch (error) {
        console.log('error', error);
      }
    };

    init();
  }, []);

  useEffect(() => {
    if (barcodes.length) {
      console.log('barcodes', barcodes);
      setDisplayBarcode(barcodes[0].rawValue || '');
    }
  }, [barcodes]);

  // useEffect(() => {
  //   console.log('rerender');
  // }, [barcodes]);

  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar
        barStyle={isDarkMode ? 'light-content' : 'dark-content'}
        backgroundColor={backgroundStyle.backgroundColor}
      />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={backgroundStyle}>
        <Header />
        <Text>{status}</Text>
        {!device ? (
          <ActivityIndicator />
        ) : (
          <>
            <Camera
              style={StyleSheet.absoluteFill}
              device={device}
              isActive={true}
              frameProcessor={frameProcessor}
              frameProcessorFps={1000000}
            />
            <Text style={[styles.barcodeTextURL, {color: 'red'}]}>
              {displayBarcode}
            </Text>
            {barcodes.map((barcode, idx) => (
              <Text key={idx} style={styles.barcodeTextURL}>
                {barcode.displayValue}
              </Text>
            ))}
          </>
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
  barcodeTextURL: {
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
  },
});

export default App;
